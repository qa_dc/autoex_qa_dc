package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {

    WebDriver driver;

    public static final String CART_URL = "https://automationexercise.com/view_cart";
    //Constructor:
    public CartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    //Selectors:
    @FindBy(xpath = "//*[b='Cart is empty!']")
    WebElement cartEmptyText;
    @FindBy(xpath = "//*[u='here']")
    WebElement clickHereToBuy;
    @FindBy(css = "[id='product-1']")
    WebElement firstProductInCart;
    @FindBy(css = "[id='product-1'] [class='cart_price'] p")
    WebElement firstProductInCartPrice;
    @FindBy(css = "[id='product-1'] [class='cart_total'] p")
    WebElement firstProductInCartTotalPrice;
    @FindBy(css = "[id='product-2'] [class='cart_price'] p")
    WebElement secondProductInCartPrice;
    @FindBy(css = "[id='product-2'] [class='cart_total'] p")
    WebElement secondProductInCartTotalPrice;
    @FindBy(css = "[id='product-1'] [class='cart_quantity'] button")
    WebElement firstProductInCartQuantity;
    @FindBy(css = "[id='product-2'] [class='cart_quantity'] button")
    WebElement secondProductInCartQuantity;
    @FindBy(css = "[id='product-2']")
    WebElement secondProductInCart;
    @FindBy(css = "[class='btn btn-default check_out']")
    WebElement proceedCheckoutButton;
    public void clickProceedCheckoutButton(){
        proceedCheckoutButton.click();
    }

    //Actions:
    public void clickHereText(){
        clickHereToBuy.click();
    }

    //Checks:
    public String checkCartEmptyText(){
        return cartEmptyText.getText();
    }
    public boolean checkFirstProductInCartIsDisplayed(){
        return firstProductInCart.isDisplayed();
    }
    public boolean checkSecondProductInCartIsDisplayed(){
        return secondProductInCart.isDisplayed();
    }

    public String checkFirstProductInCartPrice(){
        return firstProductInCartPrice.getText();
    }
    public String checkFirstProductInCartTotalPrice(){
        return firstProductInCartTotalPrice.getText();
    }
    public String checkSecondProductInCartPrice(){
        return secondProductInCartPrice.getText();
    }
    public String checkSecondProductInCartTotalPrice(){
        return secondProductInCartTotalPrice.getText();
    }
    public String checkFirstProductInCartQuantity(){
        return firstProductInCartQuantity.getText();
    }
    public String checkSecondProductInCartQuantity(){
        return secondProductInCartQuantity.getText();
    }

}
