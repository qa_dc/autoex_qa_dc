package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class AppTestingPage {
    WebDriver driver;
    public static final String API_TESTING_URL = "https://automationexercise.com/api_list";
    //Selectors:
    @FindBy(xpath = "//*[b='APIs List for practice']")
    WebElement apiTestingTitle;
    @FindBy(css = "[id='form']")
    WebElement apiTestsItems;

    //Constructor:
    public AppTestingPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    //Checks:
    public String checkApiTestingTitle(){
        return apiTestingTitle.getText();
    }
    public int countApiTestsItems(){
        List<WebElement> uTags = apiTestsItems.findElements(By.cssSelector("a[data-toggle='collapse'] > u"));
        return uTags.size();
    }



}
