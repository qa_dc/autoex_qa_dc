package pages;

import io.cucumber.java.ja.但し;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage {
    WebDriver driver;
    public static final String CONTACT_US_URL = "https://automationexercise.com/contact_us";
    //selectors:
    @FindBy(xpath = "//h2[contains(text(), 'Get In Touch')]")
    WebElement getInTouchText;
    @FindBy(css = "[data-qa='name']")
    WebElement nameInput;
    @FindBy(css = "[data-qa='email']")
    WebElement emailInput;
    @FindBy(css = "[data-qa='subject']")
    WebElement subjectInput;
    @FindBy(css = "[data-qa='message']")
    WebElement messageInput;
    @FindBy(css = "[data-qa='submit-button']")
    WebElement submitButton;
    @FindBy(css = "[class='status alert alert-success']")
    WebElement successMessage;

    //Constructor:
    public ContactUsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Actions:
    public void enterName(String name){
        nameInput.sendKeys(name);
    }
    public void enterEmail(String email){
        emailInput.sendKeys(email);
    }
    public void enterSubject(String subject){
        subjectInput.sendKeys(subject);
    }
    public void enterMessage(String message){
        messageInput.sendKeys(message);
    }
    public void clickSubmitButton(){
        submitButton.click();
    }

    //Checks:
    public boolean checkGetInTouchText(){
        return getInTouchText.isDisplayed();
    }
    public String checkSuccessMessage(){
        return successMessage.getText();
    }


}
