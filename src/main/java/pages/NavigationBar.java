package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class NavigationBar {
    WebDriver driver;
    public static final String YOUTUBE_URL = "https://www.youtube.com/c/AutomationExercise";
    //selectors
    @FindBy(css = "[class='fa fa-home']")
    WebElement homeButton;
    @FindBy(css = "[href='/products']")
    WebElement productsButton;
    @FindBy(css = "[href='/view_cart'] [class='fa fa-shopping-cart']")
    WebElement cartButton;
    @FindBy(css = "[href='/login'] [class='fa fa-lock']")
    WebElement signUpLoginButton;
    @FindBy(css = "[href='/test_cases'] [class='fa fa-list']")
    WebElement testCasesButton;
    @FindBy(css = "[href='/api_list'] [class='fa fa-list']")
    WebElement apiTestingButton;
    @FindBy(css = "[class='fa fa-envelope']")
    WebElement contactUsButton;
    @FindBy(css = "[class='fa fa-youtube-play']")
    WebElement videoTutorialsButton;
    @FindBy(css = "[href='/logout']")
    WebElement logoutButton;
    @FindBy(css = "[class='fa fa-trash-o']")
    WebElement deleteAccountButton;
    @FindBy(css = "[class='nav navbar-nav']")
    WebElement navigationBar;
    @FindBy(css = "[style='color: orange;'][href='/products']")
    WebElement productTextHighlighted;
    @FindBy(css = "[href='/'][style='color: orange;']")
    WebElement homeTextHighlighted;
    @FindBy(css = "[href='/view_cart'][style='color: orange;']")
    WebElement cartTextHighlighted;
    @FindBy(css = "[href='/login'][style='color: orange;']")
    WebElement loginTextHighlighted;
    @FindBy(css = "[href='/test_cases'][style='color: orange;']")
    WebElement testCasesTextHighlighted;
    @FindBy(css = "[href='/api_list'][style='color: orange;']")
    WebElement apiTestingTextHighlighted;
    @FindBy(css = "[href='/contact_us'][style='color: orange;']")
    WebElement contactUsTextHighlighted;
    @FindBy(css = "[aria-label='Consent'] [class='fc-button-label']")
    WebElement consentButtonPopup;

    //constructor
    public NavigationBar(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Handle popup
    public void handlePopupDialog() {
        try {
            consentButtonPopup.click();
        } catch (org.openqa.selenium.NoSuchElementException ignored) {
        }
    }

    //Actions:
    public void clickHomeButton() {
        homeButton.click();
    }

    public void clickProductsButton() {
        productsButton.click();
    }

    public void clickCartButton() {
        cartButton.click();
    }

    public void clickSignUpLoginButton() {
        signUpLoginButton.click();
    }

    public void clickTestCasesButton() {
        testCasesButton.click();
    }

    public void clickApiTestingButton() {
        apiTestingButton.click();
    }

    public void clickContactUsButton() {
        contactUsButton.click();
    }

    public void clickVideoTutorialsButton() {
        videoTutorialsButton.click();
    }

    //Checks:
    public String checkHomeTextHighlighted() {
        return homeTextHighlighted.getAttribute("style");
    }

    public String checkProductsButtonIsHighlighted() {
        return productTextHighlighted.getAttribute("style");
    }

    public String checkCartTextHighlighted() {
        return cartTextHighlighted.getAttribute("style");
    }

    public String checkLoginTextHighlighted() {
        return loginTextHighlighted.getAttribute("style");
    }

    public String checkTextCasesTextHighlighted() {
        return testCasesTextHighlighted.getAttribute("style");
    }

    public String checkApiTestingTextHighlighted() {
        return apiTestingTextHighlighted.getAttribute("style");
    }

    public String checkContactUsTextHighlighted() {
        return contactUsTextHighlighted.getAttribute("style");
    }
    public boolean  checkLogoutButtonIsDisplayed(){
        return logoutButton.isDisplayed();
    }
    public boolean checkDeleteAccountButtonIsDisplayed(){
        return deleteAccountButton.isDisplayed();
    }

    //Find all <li> elements within the list:
    public int countButtons() {
        List<WebElement> liTags = navigationBar.findElements(By.tagName("li"));
        return liTags.size();
    }

    //Extract the text for each element in the list
    public List<String> getButtonsName() {
        List<WebElement> liTags = navigationBar.findElements(By.tagName("li"));
        List<String> names = new ArrayList<>();
        for (int i = 0; i < liTags.size(); i++) {
            String text = liTags.get(i).getText();
            if (i == 1 && text.equals("" + " " + "Products")) {
                text = "Products";
            }
            names.add(text);
        }
        return names;
    }
}
