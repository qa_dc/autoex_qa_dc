package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SignUpPage {
    WebDriver driver;

    //constructor
    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public static final String SIGNUP_URL = "https://automationexercise.com/signup";
    public static final String ACCOUNT_CREATED_URL = "https://automationexercise.com/account_created";
    //selectors:
    @FindBy(xpath = "//*[@id='form']//*[b='Enter Account Information']")
    WebElement accountInfoText;
    @FindBy(xpath = "//*[@id='form']//*[b='Address Information']")
    WebElement addressInfoText;
    @FindBy(css = "[id='name'][value='User3DC']")
    WebElement nameUserInfoText;
    @FindBy(css = "[id='email'][value='User3DC@email.com']")
    WebElement emailUserInfoText;
    @FindBy(css = "[id='password']")
    WebElement passwordInput;
    @FindBy(css = "[id='first_name']")
    WebElement firstNameInput;
    @FindBy(css = "[id='last_name']")
    WebElement lastNameInput;
    @FindBy(css = "[id='address1']")
    WebElement addressInput;

    @FindBy(xpath = "[id='country']/*[option='United States']")
    WebElement countryInput;
    @FindBy(css = "[id='state']")
    WebElement stateInput;
    @FindBy(css = "[id='city']")
    WebElement cityInput;
    @FindBy(css = "[id='zipcode']")
    WebElement zipCodeInput;
    @FindBy(css = "[id='mobile_number']")
    WebElement mobileNumberInput;
    @FindBy(css = "[data-qa='create-account']")
    WebElement createAccountButton;
    @FindBy(css = "[data-qa='account-created']/*[b='Account Created!']")
    WebElement accountCreated;
    @FindBy(css = "[data-qa='continue-button']/*[a='Continue']")
    WebElement continueButton;

    //Actions:
    public void enterPassword(String password){
        passwordInput.sendKeys(password);
    }
    public void enterFirstName(String firstName){
        firstNameInput.sendKeys(firstName);
    }
    public void enterLastName(String lastName){
        lastNameInput.sendKeys(lastName);
    }
    public void enterAddress(String address){
        addressInput.sendKeys(address);
    }
    public void enterState(String state){
        stateInput.sendKeys(state);
    }
    public void enterCity(String city){
        cityInput.sendKeys(city);
    }
    public void enterZipCode(String zip){
        zipCodeInput.sendKeys(zip);
    }
    public void enterMobileNumber(String mobile){
        mobileNumberInput.sendKeys(mobile);
    }
    public void moveToAccountButtonAndClick(){
        Actions actions = new Actions(driver);
        actions.moveToElement(createAccountButton).contextClick().perform();
    }

    //Checks:
    public String checkAccountInfoText(){
        return accountInfoText.getText();
    }
    public String checkAddressInfoText(){
        return addressInfoText.getText();
    }
    public String checkNameUserInfoText(){
        return nameUserInfoText.getAttribute("value");
    }
    public String checkEmailUserInfoText(){
        return emailUserInfoText.getAttribute("value");
    }
    public String checkAccountCreatedText(){
        return accountCreated.getText();
    }
    public String checkContinueButtonDisplayed(){
        return continueButton.getText();
    }

}
