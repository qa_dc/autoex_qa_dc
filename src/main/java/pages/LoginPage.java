package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;
    //constructor
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public static final String SIGN_LOGIN_URL = "https://automationexercise.com/login";

    //selectors
    @FindBy(css= "[class='signup-form'] h2")
    WebElement newUserSignupText;
    @FindBy(css = "[class='login-form'] h2")
    WebElement loginToYourAccountText;
    @FindBy(css = "[placeholder='Name']")
    WebElement signUpNameText;
    @FindBy(css = "[data-qa='signup-email'][placeholder='Email Address']")
    WebElement signupEmailText;
    @FindBy(css = "[data-qa='login-email'][placeholder='Email Address']")
    WebElement loginEmailText;
    @FindBy (css = "[data-qa='login-password'][placeholder='Password']")
    WebElement loginPasswordText;
    @FindBy(css = "[data-qa='signup-name']")
    WebElement signupUserInput;
    @FindBy(css = "[data-qa='signup-email']")
    WebElement signupEmailInput;
    @FindBy(css = "[data-qa='login-email']")
    WebElement loginEmailInput;
    @FindBy(css = "[data-qa='login-password']")
    WebElement loginPasswordInput;
    @FindBy(css = "[data-qa='signup-button']")
    WebElement signupButton;
    @FindBy(css = "[data-qa='login-button']")
    WebElement loginButton;
    @FindBy(css = "p[style='color: red;']")
    WebElement incorrectCredentialsMessage;

    //checks
    public String checkNewUserSignupText(){
        return newUserSignupText.getText();
    }
    public String checkLoginToYourAccountText(){
        return loginToYourAccountText.getText();
    }
    public String checkSignUpNameText() {
        return signUpNameText.getAttribute("placeholder");
    }
    public String checkSignupEmailText() {
        return signupEmailText.getAttribute("placeholder");
    }
    public  String checkLoginEmailText(){
        return loginEmailText.getAttribute("placeholder");
    }
    public String checkLoginPasswordText(){
        return loginPasswordText.getAttribute("placeholder");
    }
    public boolean checkSignupButtonIsDisplayed(){
        return signupButton.isDisplayed();
    }
    public boolean checkLoginButtonIsDisplayed(){
        return loginButton.isDisplayed();
    }
    public String checkSignupButtonText(){
        return signupButton.getText();
    }
    public String checkLoginButtonText(){
        return loginButton.getText();
    }
    public String checkIncorrectCredentialsMessage(){
        return incorrectCredentialsMessage.getText();
    }


    //actions
    public void enterSignupName(String name) {
        signupUserInput.sendKeys(name);
    }
    public void enterSignupEmail(String email) {
        signupEmailInput.sendKeys(email);
    }

    public void clickSignupButton() {
        signupButton.click();
    }
    public void enterLoginEmailInput(String email) {
        loginEmailInput.sendKeys(email);
    }
    public void enterLoginPasswordInput(String password) {
        loginPasswordInput.sendKeys(password);
    }
    public void clickLoginButton(){
        loginButton.click();
    }


}
