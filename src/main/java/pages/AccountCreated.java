package pages;

import org.openqa.selenium.WebDriver;

public class AccountCreated {

    WebDriver driver;

    public AccountCreated(WebDriver driver) {
        this.driver = driver;
    }
    public static final String ACCOUNT_CREATED_URL = "https://automationexercise.com/account_created";
}
