package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage {

    WebDriver driver;

    public static final String PAYMENT_URL = "https://automationexercise.com/payment";
    public static final String PAYMENT_DONE_URL = "https://automationexercise.com/payment_done/900";
    //Constructor:

    public PaymentPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Selectors:
    @FindBy(css = "[name='name_on_card']")
    WebElement nameOnCardForm;
    @FindBy(css = "[name='card_number']")
    WebElement cardNumberForm;
    @FindBy(css = "[name='cvc']")
    WebElement cvcForm;
    @FindBy(css = "[name='expiry_month']")
    WebElement expirationMonthForm;
    @FindBy(css = "[name='expiry_year']")
    WebElement expirationYearForm;

    @FindBy(css = "[id='submit']")
    WebElement payAndConfirmButton;
    //    @FindBy(css = "#success_message div")
//    WebElement orderPlacedSuccessMessage;
//    @FindBy(css = "[id='success_message'] [class='alert-success alert']")
//    WebElement orderPlacedSuccessMessage;
    @FindBy(css = "[data-qa='order-placed'] b")
    WebElement orderPlacedSuccessMessage;



    //Actions:
    public void enterNameOnCard(String name) {
        nameOnCardForm.sendKeys(name);
    }

    public void enterCardNumber(String number) {
        cardNumberForm.sendKeys(number);
    }

    public void enterCvc(String cvc) {
        cvcForm.sendKeys(cvc);
    }

    public void enterExpirationMonth(String month) {
        expirationMonthForm.sendKeys(month);
    }

    public void enterExpirationYear(String year) {
        expirationYearForm.sendKeys(year);
    }

    public void clickPayAndConfirmButton() {
        payAndConfirmButton.click();
    }

    //Checks:
    public String checkOrderPlacedSuccessMessage() {
        return orderPlacedSuccessMessage.getText();
    }

}
