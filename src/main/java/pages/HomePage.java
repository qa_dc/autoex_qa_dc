package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage {
    WebDriver driver;
    //selectors
    @FindBy(css = "[class='logo pull-left']")
    WebElement logo;
    @FindBy(css = "[aria-label='Consent'] [class='fc-button-label']")
    WebElement consentButtonPopup;
    @FindBy(css = "#slider-carousel .carousel-inner .item > .col-sm-6 > h2")
    WebElement textSliderFull;
    @FindBy(css = "[href='#slider-carousel'] [class='fa fa-angle-left']")
    WebElement leftControlSlider;
    @FindBy(css = "[href='#slider-carousel'] [class='fa fa-angle-right']")
    WebElement rightControlSlider;
    @FindBy(css = "[class='girl img-responsive']")
    WebElement imageSlider;
    @FindBy(css = "[class='carousel-indicators']")
    WebElement sliderButtons;
    @FindBy(css = "div:nth-child(1) > div:nth-child(1) > a.test_cases_list > button")
    WebElement sliderTestCasesButton;
    @FindBy(css = "div:nth-child(2) > div:nth-child(1) > a.apis_list > button")
    WebElement sliderApiListButton;


    public static final String HOME_URL = "https://automationexercise.com/";
    public static final String TEST_CASES_URL = "https://automationexercise.com/test_cases";
    public static final String API_TEST_URL = "https://automationexercise.com/api_list";

    //constructor
    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Actions:
    public void clickLogo() {
        logo.click();
    }
    public void clickLeftControlSlider(){
        leftControlSlider.click();
    }
    public void clickSliderTestCasesButton(){
        sliderTestCasesButton.click();
    }
    public void clickSliderApiListButton(){
        sliderApiListButton.click();
    }

    //Checks:
    public String checkTextSliderFull() {
        return textSliderFull.getText();
    }
    public boolean checkImageSlider() {
        return imageSlider.isDisplayed();
    }
    public int countSliderButtons() {
        // Find all <li> elements within the list
        List<WebElement> liTags = sliderButtons.findElements(By.tagName("li"));
        return liTags.size();
    }


}
