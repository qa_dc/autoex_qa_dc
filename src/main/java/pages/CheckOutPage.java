package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckOutPage {

    WebDriver driver;

    public static final String CHECK_OUT_URL = "https://automationexercise.com/checkout";
    //Constructor:

    public CheckOutPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Selectors:
    @FindBy(css = "tr:last-child .cart_total_price")
    WebElement totalAmountToCheckout;
    @FindBy(css = "[class='btn btn-default check_out']")
    WebElement placeOrderButton;

    //Actions:
    public void moveToPlaceOrderButtonAndClick(){
        Actions actions = new Actions(driver);
        actions.moveToElement(placeOrderButton).contextClick().perform();
    }

    //Checks:
    public String checkTotalAmountToCheckout(){
        return totalAmountToCheckout.getText();
    }

}
