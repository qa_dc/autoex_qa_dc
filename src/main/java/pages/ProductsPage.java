package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Collections;
import java.util.List;

public class ProductsPage {
    WebDriver driver;

    //selectors
    @FindBy(css = "[id='search_product']")
    WebElement searchInput;
    @FindBy(css = "[id='submit_search']")
    WebElement searchButton;
    @FindBy(css = "[class='features_items'] [class='title text-center']")
    WebElement searchedProductsText;
    @FindBy(css = "[id='subscribe']")
    WebElement subscribeButton;
    @FindBy(css = "[class='single-widget'] > h2")
    WebElement subscriptionText;
    @FindBy(css = "[id='susbscribe_email']")
    WebElement subscriptionBox;
    @FindBy(css = "[class='alert-success alert']")
    WebElement subscribeSuccessMessage;
    @FindBy(css = "[class='productinfo text-center'] [data-product-id='1'] [class='fa fa-shopping-cart']")
    WebElement firstProductAddCart;
    @FindBy(css = "[class='productinfo text-center'] [data-product-id='2'] [class='fa fa-shopping-cart']")
    WebElement secondProductAddCart;
    @FindBy(css = "[class='btn btn-success close-modal btn-block']")
    WebElement continueShoppingButton;
    @FindBy(css = "[class='text-center'] [href='/view_cart']")
    WebElement viewCartButton;
    @FindBy(css = "[class='pull-left']")
    WebElement bottomPage;

    public static final String PRODUCTS_URL = "https://automationexercise.com/products";

    //constructor
    public ProductsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openPageWithCookie() {
        driver.get(PRODUCTS_URL);
        driver.manage().addCookie(new Cookie("sessionid", "gdqo5p4hv357gj4p69x23a6j65zjj6bj"));
        driver.navigate().refresh();
    }

    //Actions:
    public void enterSearchText(String searchText) {
        searchInput.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void moveToSubscribeButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(bottomPage).perform();
    }

    public void clickSubscribeButton() {
        subscribeButton.click();
    }

    public void enterEmail(String email) {
        subscriptionBox.sendKeys(email);
    }
    public void clickFirstProductAddCart(){
        firstProductAddCart.click();
    }
    public void clickSecondProductAddCart(){
        secondProductAddCart.click();
    }
    public void clickContinueShoppingButton(){
        continueShoppingButton.click();
    }
    public void clickViewCartButton(){
        viewCartButton.click();
    }

    //Checks:
    public String checkSearchedProductsText() {
        return searchedProductsText.getText();
    }

    public String checkSubscriptionText() {
        return subscriptionText.getText();
    }

    public boolean checkSubscriptionBoxIsDisplayed() {
        return subscriptionBox.isDisplayed();
    }

    public boolean checkSubscribeButtonIsDisplayed() {
        return subscribeButton.isDisplayed();
    }

    public String checkSubscribeSuccessMessage() {
        return subscribeSuccessMessage.getText();
    }

}
