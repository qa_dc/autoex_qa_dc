package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TestCasesPage {
    WebDriver driver;
    public static final String TEST_CASES_URL = "https://automationexercise.com/test_cases";
    //Selectors:
    @FindBy(xpath = "//*[b='Test Cases']")
    WebElement testCasesTitle;
    @FindBy(css = "[id='form']")
    WebElement testCasesItems;

    //Constructor
    public TestCasesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Actions:

    //Checks:
    public String checkTetCaseTitle(){
        return testCasesTitle.getText();
    }
    public int countTestCasesItems(){
        List<WebElement> uTags = testCasesItems.findElements(By.cssSelector("a[data-toggle='collapse'] > u"));
        return uTags.size();
    }


}
