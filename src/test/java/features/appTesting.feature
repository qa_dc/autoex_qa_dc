@APIAndTesting
Feature: 'API testing' page tests


  Scenario: Verify API testing page link and text
    Given Being on Home page
    When Click on API Testing button
    Then API Testing "https://automationexercise.com/api_list" page opens
    And 'APIS LIST FOR PRACTICE' text title is displayed

  @Smoke
  Scenario: Verify there are 14 api tests in the list
    Given Being on API Testing Page
    Then 14 API tests are displayed in the list


