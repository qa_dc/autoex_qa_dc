@Products
Feature: Products page navigation
  # Products page navigation feature

  Scenario: Verify Products page URL is valid
    Given Being on Home page
    When Click on Products button
    Then The products page URL is "https://automationexercise.com/products"


  Scenario Outline: Verify title after searching for a product
    Given Being on Products page
    When Enter "<product name>" in search input and click search button
    Then Result title  is "SEARCHED PRODUCTS"
    Examples:
      | product name |
      | Jeans        |

  Scenario: Verify subscriptions text and fields
    Given Being on Products page
    When Scroll down to footer
    Then "SUBSCRIPTION" is displayed
    And Email text box and subscribe button are visible

  @Smoke @Regression
  Scenario: Verify subscribe action
    Given Being on Products page footer
    When Enter "User3DC@email.com" email address
    And Click arrow button
    Then 'You have been successfully subscribed!' message is visible






