@Login
Feature: Signup/Login
  # Signup / Login page sections:
  # 'New User signup' section, with 'Name', 'Email' and 'Signup' button (goes to account registration)
  # 'Login to your account' section, with 'Email', 'Password' and 'Login' button


  Scenario: Check if "Login to your account" and "New User Signup!" are displayed and correct
    Given Being on Signup Login page
    Then "New User Signup!" and "Login to your account" labels are correctly displayed

  Scenario: Check tooltip text inside 'Name' and 'Email' text boxes
    Given Being on Signup Login page
    Then Tooltip "Name" respectively "Email Address" appears in signup section
    And Tooltip "Email Address" respectively "Password" appears in login section

  Scenario: Check if 'Login' and 'Signup' buttons are displayed with the corresponding text
    Given Being on Signup Login page
    Then Login and Signup buttons are displayed
    And "Login" and "Signup" text appear on buttons

  @Smoke @Regression
  Scenario: Check if Login works with correct credentials
    Given Being on Signup Login page
    When Enter email and password
    And Click on Login button
    Then Logout and Delete Account buttons are displayed

  @Negative
  Scenario: Check Login doesn't work with correct email and wrong password
    Given Being on Signup Login page
    When Enter email and "WrongPassword"
    And Click on Login button
    Then Message "Your email or password is incorrect!" is displayed

  @Negative
  Scenario: Check Login doesn't work with wrong email and correct password
    Given Being on Signup Login page
    When Enter "wrong@email.com" and password
    And Click on Login button
    Then Message "Your email or password is incorrect!" is displayed

  @Negative
  Scenario: Check Login doesn't work with wrong email password
    Given Being on Signup Login page
    When Enter "wrong@email.com" and "WrongPassword"
    And Click on Login button
    Then Message "Your email or password is incorrect!" is displayed




