@Signup
Feature: Signup
  # Signup page
  # 'Enter account information' and 'Address information'

  Background: Initial data for signup
    Given Navigate to login page
    And Signup "User3DC" user and "User3DC@email.com" email and press Signup

  Scenario: Check if "Enter account information" and "Address Information" titles are displayed
#    Given Being on Signup page with name and email
    Then "ENTER ACCOUNT INFORMATION" and "ADDRESS INFORMATION" texts are displayed
    And Name and Email fields are filled in with "User3DC" respectively "User3DC@email.com"

  @Smoke @Regression
  Scenario Outline: Register a user with mandatory fields
#    Given Being on Signup page with name and email
    When Enter mandatory fields with correct data
      | <password>     |
      | <firstname>    |
      | <lastname>     |
      | <address>      |
      | <state>        |
      | <city>         |
      | <zipcode>      |
      | <mobilenumber> |

    And Click Create account
    Then "ACCOUNT CREATED!" message and "Continue" button are displayed
    Examples:
      | password | firstname | lastname | address     | state   | city  | zipcode | mobilenumber |
      | User3DC  | Daniel    | Canur    | Ocean Drive | Florida | Miami | 700322  | 0742112211   |


