@ContactUs
Feature: Contact us form tests

  Scenario: Check navigation to 'Contact us' page
    Given Being on Home page
    When Click on Contact us button
    Then Contact us "https://automationexercise.com/contact_us" page opens
    And GET IN TOUCH text is displayed

  @Smoke @Regression
  Scenario Outline: Submit a message
    Given Being on Contact us page
    When Fill in all fields
      | <name>    |
      | <email>   |
      | <subject> |
      | <message> |
    And Click Submit button
    Then "Success! Your details have been submitted successfully." is visible

    Examples:
      | name    | email             | subject       | message       |
      | User3DC | User3DC@email.com | User3 subject | User3 message |