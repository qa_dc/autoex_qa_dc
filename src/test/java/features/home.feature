@Home
Feature: Home page
    # Home page navigation feature


  Scenario: Verify Home page URL is valid
    Given Being on Home page
    When Click on logo
    Then The home page URL is "https://automationexercise.com/"


  Scenario: Verify navigation bar options on Home page with no user logged in
    Given Being on Home page
    When Click on Home button
    Then Navigation bar displays all 8 options in navigation bar

  Scenario: Verify slider carousel has all 3 indicators
    Given Being on Home page
    When Click on left control
    Then Number of slides is 3


  Scenario Outline: Verify text and images in slider carousel
    Given Being on Home page
    When Click on Home button
    Then Slider "<text>" is correct
    And Image slides are displayed
    Examples:
      | text                                                   |
      | Full-Fledged practice website for Automation Engineers |

  @Smoke
  Scenario: Verify if 'Test Cases' button in slider carousel works
    Given Being on Home page
    When Click on Test Cases button in slider
    Then Test cases page opens "https://automationexercise.com/test_cases"


  Scenario: Verify if 'APIs list' button in slider carousel works
    Given Being on Home page
    When Click on APIs list button in slider
    Then Api list page opens "https://automationexercise.com/api_list"


