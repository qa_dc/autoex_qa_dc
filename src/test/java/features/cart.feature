@Cart
Feature: Cart page tests

  @Smoke
  Scenario: Check an empty cart page
    Given Being on Products page
    When Click on Cart button
    Then Cart "https://automationexercise.com/view_cart" page opens
    And "Cart is empty! Click here to buy products." text is displayed

  @Regression
  Scenario: Verify the link to the products page
    Given Being on Cart page
    When Click on here text
    Then Products "https://automationexercise.com/products" page opens

  @Regression
  Scenario: Add products in Cart
    Given User logged in and navigate to Products page
    When Select first product and click Add to cart
    And Click Continue Shopping button
    And Select the second product and click Add to cart
    And Click View Cart button
    Then Both products are added to Cart
    And Prices, quantity and total price are correct
    And Total amount in checkout page is correct

  @Regression
  Scenario Outline: Pay and confirm order
    Given User logged in and navigate to Products page
    And Navigate to Checkout page
    When Press Place Order button
    And Enter payment details
      | <CardName> |
      | <CardNo>   |
      | <cvc>      |
      | <ExpMonth> |
      | <ExpYear>  |
    And Press Pay and Confirm Order
    Then 'ORDER PLACED!' message is displayed
    Examples:
      | CardName | CardNo | cvc | ExpMonth | ExpYear |
      | DC       | 1111   | 111 | 12       | 12      |
