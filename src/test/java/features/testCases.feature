@APIAndTesting
Feature: 'Test Cases' page tests

  @Smoke
  Scenario: Verify test cases page link and text
    Given Being on Products page
    When Click on Test Cases button
    Then Test Cases "https://automationexercise.com/test_cases" page opens
    And "TEST CASES"  text is displayed

  Scenario: Verify there are 26 test cases in the list
    Given Being on Test Cases Page
    Then 26 test cases are displayed in the list