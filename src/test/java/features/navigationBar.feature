@NavigationBar
Feature:Navigation bar options with no user logged in
  # Navigation bar available on all pages:
  # Home, Products, Cart, Signup / Login,
  # Test Cases, API Testing, Video Tutorials, Contact us

  Scenario: Check 'Products' page URL and 'Products' button color
    Given Being on Home page
    When Click on Products button
    Then Products "https://automationexercise.com/products" page opens
    And Products text is orange

  Scenario: Check 'Home' page URL and 'Home' button color
    Given Being on Products page
    When Click on Home button
    Then Home "https://automationexercise.com/" page opens
    And Home text is orange

  Scenario: Check 'Cart' page URL and 'Cart' button color
    Given Being on Home page
    When Click on Cart button
    Then Cart "https://automationexercise.com/view_cart" page opens
    And Cart text is orange

  Scenario:  Check 'Signup/Login' page URL and 'Signup/Login' button color
    Given Being on Home page
    When Click on Signup Login button
    Then Login "https://automationexercise.com/login" page opens
    And Signup Login text is orange

  Scenario: Check 'Test Cases' page URL and 'Test Cases' button color
    Given Being on Home page
    When Click on Test Cases button
    Then Test Cases "https://automationexercise.com/test_cases" page opens
    And Test Cases text is orange

  Scenario: Check 'API Testing' page URL and 'API Testing' button color
    Given Being on Home page
    When Click on API Testing button
    Then API Testing "https://automationexercise.com/api_list" page opens
    And API Testing text is orange

  Scenario: Check 'Contact us' page URL and 'Contact us' button color
    Given Being on Home page
    When Click on Contact us button
    Then Contact US "https://automationexercise.com/contact_us" page opens
    And Contact us text is orange

  Scenario: Check Youtube page is opened when 'Video Tutorials' button is pressed
    Given Being on Home page
    When Click on Video Tutorials button
    Then "https://www.youtube.com/c/AutomationExercise" page opens

  @Smoke @Regression
  Scenario Outline: Verify navigation bar options names
    Given Being on Home page
    When Click on Home button
    Then The list options should be '<options>'

    Examples:
      | options                                                                                      |
      | [Home, Products, Cart, Signup / Login, Test Cases, API Testing, Video Tutorials, Contact us] |
