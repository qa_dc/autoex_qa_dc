@APIRest
Feature: API Rest testing

  Scenario: Verify status code for bookings list is 200
    Given A valid API endpoint for booking list
    When Send a GET request
    Then Receive a status code 200

  Scenario: Verify status code for booking by ID request
    Given A valid API endpoint for booking
    When Send a GET request for booking with ID = 12
    Then Status code should be 200

  Scenario: Verify booking first and last name is correct
    Given A valid API endpoint for booking
    When Send a GET request for booking with ID = 12
    Then Booking should have first name "John" and last name "Smith" and check-in date "2018-01-01"

  Scenario: Verify GET request with incorrect ID returns 404
    Given A valid API endpoint for booking
    When Send a GET request for booking with incorrect ID = 12353
    Then Status code should be 404
    And Response body should be "Not Found"

  Scenario: Verify POST request returns 200
    Given A valid API endpoint for booking
    When Send a POST request to create a booking
    Then Status code should be 200
    And Response body should contain the booking firstname details

  Scenario: Verify DELETE request returns 403
    Given A valid API endpoint for booking
    And Authorization header with value "Basic YWRtaW46cGFzc3dvcmQxMjM="
    When I send a DELETE request to delete a booking with ID = 2071
    Then Status code should be 403
