package runners;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "stepdefinitions",
        features = "src/test/java/features",
        tags = "@Home",
        publish = true,
        plugin = {"pretty", "html:target/HomeTest.html"}
)


public class HomeRunner {
}
