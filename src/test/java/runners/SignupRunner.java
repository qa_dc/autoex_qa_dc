package runners;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "stepdefinitions",
        features = "src/test/java/features",
        tags = "@Signup",
        publish = true,
        plugin = {"pretty", "html:target/SignupTest.html"}
)


public class SignupRunner {
}
