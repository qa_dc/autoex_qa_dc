package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseTestClass {
    protected WebDriver driver;
    protected static Logger logger = Logger.getLogger(BaseTestClass.class.getName());

    @Before
    public void setupChrome() {
        driver = new ChromeDriver();
        logger.log(Level.INFO, "Chrome browser opened");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }

    @After
    public void quitChrome() {
        driver.quit();
        logger.log(Level.INFO, "Chrome browser closed");
    }
}
