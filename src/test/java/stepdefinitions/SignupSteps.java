package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.*;

import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class SignupSteps {

    SignUpPage signUpPage;
    LoginPage loginPage;

    @Given("Navigate to login page")
    public void navigateToLoginPage() {
        loginPage = new LoginPage(driver);
        driver.get(LoginPage.SIGN_LOGIN_URL);
    }

    @And("Signup {string} user and {string} email and press Signup")
    public void signupUserAndEmail(String name, String email) {
        loginPage.enterSignupName(name);
        loginPage.enterSignupEmail(email);
        loginPage.clickSignupButton();
        signUpPage = new SignUpPage(driver);
    }

//    @Given("Being on Signup page with name and email")
//    public void beingOnSignupPageWithNameAndEmail() {
//    }

    @Then("{string} and {string} texts are displayed")
    public void checkTitlesInSignupPage(String text1, String text2) {
        logger.log(Level.INFO, "Check titles on Signup page");
        Assert.assertEquals("'Text' missing or incorrect",
                text1,
                signUpPage.checkAccountInfoText());
        Assert.assertEquals("'Text' missing or incorrect",
                text2,
                signUpPage.checkAddressInfoText());
    }

    @And("Name and Email fields are filled in with {string} respectively {string}")
    public void userAndEmailFilledInSignupPage(String text1, String text2) {
        logger.log(Level.INFO, "Check if Name and Email fields are filled in");
        Assert.assertEquals("'Name' field is not filled in",
                text1,
                signUpPage.checkNameUserInfoText());
        Assert.assertEquals("'Email' field is not filled in",
                text2,
                signUpPage.checkEmailUserInfoText());
    }

    @When("Enter mandatory fields with correct data")
    public void enterMandatoryFieldsWithCorrectData(List<String> inputs) {
        signUpPage.enterPassword(inputs.get(0));
        signUpPage.enterFirstName(inputs.get(1));
        signUpPage.enterLastName(inputs.get(2));
        signUpPage.enterAddress(inputs.get(3));
        signUpPage.enterState(inputs.get(4));
        signUpPage.enterCity(inputs.get(5));
        signUpPage.enterZipCode(inputs.get(6));
        signUpPage.enterMobileNumber(inputs.get(7));
    }

    @And("Click Create account")
    public void clickCreateAccount() {
        signUpPage.moveToAccountButtonAndClick();
        driver.get(AccountCreated.ACCOUNT_CREATED_URL);
    }

    @Then("{string} message and {string} button are displayed")
    public void messageAndContinueButtonDisplayed(String text1, String text2) {
        logger.log(Level.INFO, "Check if Name and Email fields are filled in");
        Assert.assertEquals("'Account created!' text missing",
                text1,
                signUpPage.checkAccountCreatedText());
        Assert.assertEquals("'Continue' button missing",
                text2,
                signUpPage.checkContinueButtonDisplayed());
    }

}
