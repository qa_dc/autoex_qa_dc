package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.*;

import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class CartSteps {

    CartPage cartPage;
    ProductsPage productsPage;
    LoginPage loginPage;
    HomePage homePage;
    NavigationBar navigationBar;
    CheckOutPage checkOutPage;
    PaymentPage paymentPage;

    public String testUser = "User1DC@email.com";
    public String password = "User1";

    @And("{string} text is displayed")
    public void checkEmptyCartText(String text) {
        logger.log(Level.INFO, "Check 'Cart is empty' text");
        cartPage = new CartPage(driver);
        Assert.assertEquals("'Cart is empty' text missing or misspelled",
                text,
                cartPage.checkCartEmptyText());
    }

    @Given("Being on Cart page")
    public void beingOnCartPage() {
        driver.get(CartPage.CART_URL);
        logger.log(Level.INFO, "Being on Cart page");
    }

    @When("Click on here text")
    public void clickOnHereText() {
        cartPage = new CartPage(driver);
        cartPage.clickHereText();
    }

    @Given("User logged in and navigate to Products page")
    public void userLoggedInAndNavigateToProductsPage() {
        loginPage = new LoginPage(driver);
        driver.get(LoginPage.SIGN_LOGIN_URL);
        loginPage.enterLoginEmailInput(testUser);
        loginPage.enterLoginPasswordInput(password);
        loginPage.clickLoginButton();
        homePage = new HomePage(driver);
        navigationBar = new NavigationBar(driver);
        navigationBar.clickProductsButton();
        productsPage = new ProductsPage(driver);
    }
    @When("Select first product and click Add to cart")
    public void selectFirstProductAndClickAddToCart() {
        productsPage = new ProductsPage(driver);
        logger.log(Level.INFO, "Add 1st product to cart");
        productsPage.clickFirstProductAddCart();
    }
    @And("Click Continue Shopping button")
    public void clickContinueShoppingButton() {
        productsPage.clickContinueShoppingButton();
        logger.log(Level.INFO, "Click continue to shopping");
    }
    @And("Select the second product and click Add to cart")
    public void selectTheSecondProductAndClickAddToCart() {
        logger.log(Level.INFO, "Add 2nd product to cart");
        productsPage.clickSecondProductAddCart();
    }
    @And("Click View Cart button")
    public void clickViewCartButton() {
        productsPage.clickViewCartButton();
        logger.log(Level.INFO, "Click vie cart button");
        cartPage = new CartPage(driver);
    }
    @Then("Both products are added to Cart")
    public void checkBothProductsAreAddedToCart() {
        driver.get(CartPage.CART_URL);
        logger.log(Level.INFO, "Check 2 products in cart");
        Assert.assertTrue("1st Product is not in cart",
                cartPage.checkFirstProductInCartIsDisplayed());
        Assert.assertTrue("2nd Product is not in cart",
                cartPage.checkSecondProductInCartIsDisplayed());
    }
    @And("Prices, quantity and total price are correct")
    public void checkPricesQuantityAndTotalPrice() {
        logger.log(Level.INFO, "Check price and qty/total.qty");
        Assert.assertEquals("Wrong price for the 1st product",
                "Rs. 500",
                cartPage.checkFirstProductInCartPrice());
        Assert.assertEquals("Wrong price for the 2nd product",
                "Rs. 400",
                cartPage.checkSecondProductInCartPrice());
        Assert.assertEquals("Wrong quantity for the 1st product",
                "1",
                cartPage.checkFirstProductInCartQuantity());
        Assert.assertEquals("Wrong quantity for the 2nd product",
                "1",
                cartPage.checkSecondProductInCartQuantity());
        Assert.assertEquals("Wrong total price for the 1st product",
                "Rs. 500",
                cartPage.checkFirstProductInCartTotalPrice());
        Assert.assertEquals("Wrong total price for the 2nd product",
                "Rs. 400",
                cartPage.checkSecondProductInCartTotalPrice());
    }
    @And("Total amount in checkout page is correct")
    public void checkTotalAmountInCheckoutPageIsCorrect() {
        cartPage.clickProceedCheckoutButton();
        checkOutPage = new CheckOutPage(driver);
        checkOutPage.checkTotalAmountToCheckout();
    }
    @And("Navigate to Checkout page")
    public void navigateToCheckoutPage() {
        driver.get(ProductsPage.PRODUCTS_URL);
        navigationBar = new NavigationBar(driver);
        navigationBar.clickCartButton();
        driver.get(CartPage.CART_URL);
        cartPage = new CartPage(driver);
        cartPage.clickProceedCheckoutButton();
        driver.get(CheckOutPage.CHECK_OUT_URL);
    }
    @When("Press Place Order button")
    public void pressPlaceOrderButton() {
        checkOutPage = new CheckOutPage(driver);
        checkOutPage.moveToPlaceOrderButtonAndClick();
        paymentPage = new PaymentPage(driver);
    }
    @And("Enter payment details")
    public void enterPaymentDetails(List<String> inputs) {
        driver.get(PaymentPage.PAYMENT_URL);
        paymentPage.enterNameOnCard(inputs.get(0));
        paymentPage.enterCardNumber(inputs.get(1));
        paymentPage.enterCvc(inputs.get(2));
        paymentPage.enterExpirationMonth(inputs.get(3));
        paymentPage.enterExpirationYear(inputs.get(4));
    }
    @And("Press Pay and Confirm Order")
    public void pressPayAndConfirmOrder() {
        paymentPage.clickPayAndConfirmButton();
    }

    @Then("{string} message is displayed")
    public void orderSuccessfullyMessageIsDisplayed(String text) {
        driver.get(PaymentPage.PAYMENT_DONE_URL);
        Assert.assertEquals("Message missing or misspelled",
                text,
                paymentPage.checkOrderPlacedSuccessMessage());
    }


}
