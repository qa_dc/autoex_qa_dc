package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.TestCasesPage;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class TestCasesSteps {

    TestCasesPage testCasesPage;

    @And("{string}  text is displayed")
    public void textIsDisplayed(String text) {
        logger.log(Level.INFO, "Check 'TEST CASE' text");
        testCasesPage = new TestCasesPage(driver);
        Assert.assertEquals("'TEST CASE' text missing or misspelled",
                text,
                testCasesPage.checkTetCaseTitle());
    }

    @Given("Being on Test Cases Page")
    public void beingOnTestCasesPage() {
        driver.get(TestCasesPage.TEST_CASES_URL);
        logger.log(Level.INFO, "Being on API Testing page");
        testCasesPage = new TestCasesPage(driver);
    }

    @Then("{int} test cases are displayed in the list")
    public void checkNumberOfTestCasesInTheList(int i) {
        logger.log(Level.INFO, "Verify there are 26 test cases in the list");
        Assert.assertEquals("There are no 26 test cases in the list",
                i,
                testCasesPage.countTestCasesItems());
    }
}
