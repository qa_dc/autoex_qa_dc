package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.AppTestingPage;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class AppTestingSteps {

    AppTestingPage apptestingPage;

    @And("{string} text title is displayed")
    public void textIsDisplayed(String text) {
        logger.log(Level.INFO, "Check 'APIS LIST FOR PRACTICE' text");
        apptestingPage = new AppTestingPage(driver);
        Assert.assertEquals("'TEST CASE' text missing or misspelled",
                text,
                apptestingPage.checkApiTestingTitle());
    }

    @Given("Being on API Testing Page")
    public void beingOnAPITestingPage() {
        driver.get(AppTestingPage.API_TESTING_URL);
        logger.log(Level.INFO, "Being on API Testing page");
        apptestingPage = new AppTestingPage(driver);
    }

    @Then("{int} API tests are displayed in the list")
    public void checkNumberOfApiTestsInTheList(int i) {
        logger.log(Level.INFO, "Verify there are 14 api tests in the list");
        Assert.assertEquals("There are no 14 API tests in the list",
                i,
                apptestingPage.countApiTestsItems());
    }

}
