package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.*;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class NavigationBarSteps {
    NavigationBar navigationBar;


    @When("Click on Products button")
    public void clickOnProductsButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on Products text");
        navigationBar.clickProductsButton();
    }

    @Then("Products {string} page opens")
    public void productsPageOpens(String text) {
        logger.log(Level.INFO, "Check 'Products' page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                ProductsPage.PRODUCTS_URL);
    }

    @And("Products text is orange")
    public void productsTextIsOrange() {
        logger.log(Level.INFO, "Check if Products text is highlighted");
        Assert.assertEquals("Products text is not orange",
                "color: orange;",
                navigationBar.checkProductsButtonIsHighlighted());
    }

    @Given("Being on Products page")
    public void beingOnProductsPage() {
        driver.get(ProductsPage.PRODUCTS_URL);
        logger.log(Level.INFO, "Being on Products page");
    }

    @Then("Home {string} page opens")
    public void homePageOpens(String text) {
        logger.log(Level.INFO, "Check 'Home' page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                HomePage.HOME_URL);
    }

    @And("Home text is orange")
    public void homeTextIsOrange() {
        logger.log(Level.INFO, "Check if Home text is highlighted");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Home text is not orange",
                "color: orange;",
                navigationBar.checkHomeTextHighlighted());
    }

    @When("Click on Cart button")
    public void clickOnCartButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on Cart button");
//        navigationBar.handlePopupDialog();
        navigationBar.clickCartButton();
    }

    @Then("Cart {string} page opens")
    public void cartPageOpens(String text) {
        logger.log(Level.INFO, "Check 'Cart' page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                CartPage.CART_URL);
    }

    @And("Cart text is orange")
    public void cartTextIsOrange() {
        logger.log(Level.INFO, "Check if Cart text is highlighted");
        Assert.assertEquals("Cart button is not orange",
                "color: orange;",
                navigationBar.checkCartTextHighlighted());
    }

    @When("Click on Signup Login button")
    public void clickOnSignupLoginButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on Signup button");
//        navigationBar.handlePopupDialog();
        navigationBar.clickSignUpLoginButton();
    }

    @Then("Login {string} page opens")
    public void loginPageOpens(String text) {
        logger.log(Level.INFO, "Check 'Login' page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                LoginPage.SIGN_LOGIN_URL);
    }

    @And("Signup Login text is orange")
    public void signupLoginButtonIsOrange() {
        Assert.assertEquals("Signup/Login text is not orange",
                "color: orange;",
                navigationBar.checkLoginTextHighlighted());
    }

    @When("Click on Test Cases button")
    public void clickOnTestCasesButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on Test Cases button");
//        navigationBar.handlePopupDialog();
        navigationBar.clickTestCasesButton();
    }

    @Then("Test Cases {string} page opens")
    public void testCasesPageOpens(String text) {
        logger.log(Level.INFO, "Check 'Test cases' page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                TestCasesPage.TEST_CASES_URL);
    }

    @And("Test Cases text is orange")
    public void testCasesButtonIsOrange() {
        logger.log(Level.INFO, "Check Test Cases button color");
        Assert.assertEquals("Test Cases text is not orange",
                "color: orange;",
                navigationBar.checkTextCasesTextHighlighted());
    }

    @When("Click on API Testing button")
    public void clickOnAPITestingButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on API Testing button");
//        navigationBar.handlePopupDialog();
        navigationBar.clickApiTestingButton();
    }

    @Then("API Testing {string} page opens")
    public void apiTestingPageOpens(String text) {
        logger.log(Level.INFO, "Check 'API Testing' page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                AppTestingPage.API_TESTING_URL);
    }

    @And("API Testing text is orange")
    public void apiTestingButtonIsOrange() {
        Assert.assertEquals("Api Test text is not orange",
                "color: orange;",
                navigationBar.checkApiTestingTextHighlighted());
    }

    @When("Click on Contact us button")
    public void clickOnContactUsButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on Contact us button");
//        navigationBar.handlePopupDialog();
        navigationBar.clickContactUsButton();
    }

    @Then("Contact US {string} page opens")
    public void contactUSPageOpens(String text) {
        logger.log(Level.INFO, "Check 'Contact us page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                ContactUsPage.CONTACT_US_URL);
    }

    @And("Contact us text is orange")
    public void contactUsButtonIsOrange() {
        Assert.assertEquals("Contact us text is not orange",
                "color: orange;",
                navigationBar.checkContactUsTextHighlighted());
    }

    @When("Click on Video Tutorials button")
    public void clickOnVideoTutorialsButton() {
        navigationBar = new NavigationBar(driver);
        logger.log(Level.INFO, "Click on Video tutorials button");
//        navigationBar.handlePopupDialog();
        navigationBar.clickVideoTutorialsButton();
    }

    @Then("{string} page opens")
    public void youtubePageOpens(String text) {
        logger.log(Level.INFO, "Check YouTube page URL");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Invalid URL",
                text,
                NavigationBar.YOUTUBE_URL);
    }

    @Then("The list options should be {string}")
    public void checkButtonsNavigationBar(String expectedNames) {
        logger.log(Level.INFO, "Check navigation bar option names");
        navigationBar = new NavigationBar(driver);
        Assert.assertEquals("Options names on navigation bar are not correct",
                expectedNames,
                navigationBar.getButtonsName().toString());
    }

}

