package stepdefinitions;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.HomePage;
import pages.LoginPage;
import pages.NavigationBar;
import pages.SignUpPage;

import javax.lang.model.element.Name;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class LoginSteps {
    LoginPage loginPage;
    SignUpPage signUpPage;
    HomePage homePage;
    NavigationBar navigationBar;
    public String testUser = "User1DC@email.com";
    public String password = "User1";

    @Given("Being on Signup Login page")
    public void beingOnSignupLoginPage() {
        loginPage = new LoginPage(driver);
        driver.get(LoginPage.SIGN_LOGIN_URL);
        logger.log(Level.INFO, "Being on login page");
    }
    @Then("{string} and {string} labels are correctly displayed")
    public void labelsAreDisplayed(String text1, String text2) {
        loginPage = new LoginPage(driver);
        Assert.assertEquals("'Login to your account'- syntax error",
                text1,
                loginPage.checkNewUserSignupText());
        Assert.assertEquals("'New User Signup!'- syntax error",
                text2,
                loginPage.checkLoginToYourAccountText());
    }
    @Then("Tooltip {string} respectively {string} appears in signup section")
    public void tooltipsAppearInSignupSection(String text1, String text2) {
        logger.log(Level.INFO, "Check if 'Name' and 'Email' text boxes are filled in");
        loginPage = new LoginPage(driver);
        Assert.assertEquals("There is no 'Name' filled in Name text box",
                text1,
                loginPage.checkSignUpNameText());
        Assert.assertEquals("There is no 'Email Address' filled in Name text box",
                text2,
                loginPage.checkSignupEmailText());
    }
    @And("Tooltip {string} respectively {string} appears in login section")
    public void tooltipsAppearInLoginSection(String text1, String text2) {
        logger.log(Level.INFO, "Check if 'Email' and 'Password' text boxes are filled in");
        loginPage = new LoginPage(driver);
        Assert.assertEquals("There is no 'Email Address' tooltip in Name text box",
                text1,
                loginPage.checkLoginEmailText());
        Assert.assertEquals("There is no 'Email Address' tooltip in Name text box",
                text2,
                loginPage.checkLoginPasswordText());
    }
    @Then("Login and Signup buttons are displayed")
    public void buttonsAreDisplayed() {
        logger.log(Level.INFO, "'Login' and 'Signup' buttons are displayed ?");
        Assert.assertTrue("Login button is not displayed",
                loginPage.checkLoginButtonIsDisplayed());
        Assert.assertTrue("Signup button is not displayed",
                loginPage.checkSignupButtonIsDisplayed());
    }
    @And("{string} and {string} text appear on buttons")
    public void textAppearOnButtons(String text1, String text2) {
        logger.log(Level.INFO, "'Login' and 'Signup' text correct ?");
        Assert.assertEquals("'Login' text missing or misspelled",
                text1,
                loginPage.checkLoginButtonText());
        Assert.assertEquals("'Signup' text missing or misspelled",
                text2,
                loginPage.checkSignupButtonText());
    }

    @When("Enter email and password")
    public void enterEmailAndPassword() {
        loginPage.enterLoginEmailInput(testUser);
        loginPage.enterLoginPasswordInput(password);
        logger.log(Level.INFO, "Entering email and password");
    }

    @And("Click on Login button")
    public void clickOnLoginButton() {
        loginPage.clickLoginButton();
        logger.log(Level.INFO, "Click on login button");
        homePage = new HomePage(driver);
    }

    @Then("Logout and Delete Account buttons are displayed")
    public void logoutAndDeleteButtonsDisplayed() {
        logger.log(Level.INFO, "'Logout' and 'Delete Account' are displayed");
        navigationBar = new NavigationBar(driver);
        Assert.assertTrue("'Logout' button missing",
                navigationBar.checkLogoutButtonIsDisplayed());
        Assert.assertTrue("'Delete account' button missing",
                navigationBar.checkDeleteAccountButtonIsDisplayed());
    }

    @When("Enter email and {string}")
    public void enterEmailAndWrongPassword(String text) {
        logger.log(Level.INFO, "Enter correct email and wrong password");
        loginPage.enterLoginEmailInput(testUser);
        loginPage.enterLoginPasswordInput(text);
    }

    @Then("Message {string} is displayed")
    public void incorrectCredentialMessageDisplayed(String text) {
        logger.log(Level.INFO, "'Your email or password is incorrect!' message displayed");
        Assert.assertEquals("Message is not displayed or misspelled",
                text,
                loginPage.checkIncorrectCredentialsMessage());
    }

    @When("Enter {string} and password")
    public void enterWrongEmailAndPassword(String text) {
        logger.log(Level.INFO, "Enter wrong email and correct password");
        loginPage.enterLoginEmailInput(text);
        loginPage.enterLoginPasswordInput(password);

    }
    @When("Enter {string} and {string}")
    public void enterWrongEmailAndWrongPassword(String text1, String text2) {
        logger.log(Level.INFO, "Enter wrong email and password");
        loginPage.enterLoginEmailInput(text1);
        loginPage.enterLoginPasswordInput(text2);
    }

}




