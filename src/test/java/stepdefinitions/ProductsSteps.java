package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import pages.*;

import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class ProductsSteps {

    ProductsPage productsPage;


    @Then("The products page URL is {string}")
    public void theProductsPageURLShouldBe(String text) {
        logger.log(Level.INFO, "Check Products page URL");
        productsPage = new ProductsPage(driver);
        Assert.assertEquals("Invalid URL",
                text,
                driver.getCurrentUrl());
    }

    @When("Enter {string} in search input and click search button")
    public void enterInSearchInputAndClickSearchButton(String text) {
        productsPage = new ProductsPage(driver);
        productsPage.openPageWithCookie();
        productsPage.enterSearchText(text);
        productsPage.clickSearchButton();
    }

    @Then("Result title  is {string}")
    public void resultTitle(String text) {
        Assert.assertEquals("'SEARCHED PRODUCTS' title misspelled or missing",
                text,
                productsPage.checkSearchedProductsText());
    }

    @When("Scroll down to footer")
    public void scrollDownToFooter() {
        productsPage = new ProductsPage(driver);
        logger.log(Level.INFO, "Move to subscribe footer");
        productsPage.moveToSubscribeButton();
    }

    @Then("{string} is displayed")
    public void subscriptionTextIsDisplayed(String text) {
        logger.log(Level.INFO, "Check 'Subscription' text");
        Assert.assertEquals("Subscription' is not visible or misspelled",
                text,
                productsPage.checkSubscriptionText());
    }

    @And("Email text box and subscribe button are visible")
    public void emailBoxAndSubscribeButtonAreVisible() {
        logger.log(Level.INFO, "Subscribe box and button button");
        Assert.assertTrue("Subscribe box is not displayed", productsPage.checkSubscriptionBoxIsDisplayed());
        Assert.assertTrue("Subscribe button is not displayed", productsPage.checkSubscribeButtonIsDisplayed());
    }

    @Given("Being on Products page footer")
    public void beingOnProductsPageFooter() {
        driver.get(ProductsPage.PRODUCTS_URL);
        productsPage = new ProductsPage(driver);
        logger.log(Level.INFO, "Being on Products page");
        productsPage.moveToSubscribeButton();
    }

    @When("Enter {string} email address")
    public void enterEmailAddress(String text) {
        productsPage.enterEmail(text);
    }

    @And("Click arrow button")
    public void clickArrowButton() {
        productsPage.clickSubscribeButton();
    }

    @Then("{string} message is visible")
    public void messageSubmittedSuccessfullyIsVisible(String message) {
        logger.log(Level.INFO, "Check success message");
        Assert.assertEquals("Success message text is missing or misspelled",
                message,
                productsPage.checkSubscribeSuccessMessage());
    }

}
