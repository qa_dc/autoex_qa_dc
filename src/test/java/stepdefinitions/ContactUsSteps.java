package stepdefinitions;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.ContactUsPage;
import pages.HomePage;

import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class ContactUsSteps {

    ContactUsPage contactUsPage;

    @Then("Contact us {string} page opens")
    public void checkContactUsPageLink(String text) {
        logger.log(Level.INFO, "Check 'Contact us page URL");
        Assert.assertEquals("Invalid URL",
                text,
                ContactUsPage.CONTACT_US_URL);
    }

    @And("GET IN TOUCH text is displayed")
    public void checkGetInTouchTextDisplayed() {
        contactUsPage = new ContactUsPage(driver);
        logger.log(Level.INFO, "Check if 'GET IN TOUCH' is displayed");
        Assert.assertTrue("'GET IN TOUCH' text is missing",
                contactUsPage.checkGetInTouchText());

    }

    @Given("Being on Contact us page")
    public void beingOnContactUsPage() {
        driver.get(ContactUsPage.CONTACT_US_URL);
        logger.log(Level.INFO, "Being on 'Contact us' page");
        contactUsPage = new ContactUsPage(driver);
    }

    @When("Fill in all fields")
    public void fillInAllFields(List<String> inputs) {
        contactUsPage.enterName(inputs.get(0));
        contactUsPage.enterEmail(inputs.get(1));
        contactUsPage.enterSubject(inputs.get(2));
        contactUsPage.enterMessage(inputs.get(3));
    }

    @And("Click Submit button")
    public void clickSubmitButton() {
        contactUsPage.clickSubmitButton();
        driver.switchTo().alert().accept();
    }

    @Then("{string} is visible")
    public void messageSubmittedSuccessfullyIsVisible(String text) {
        logger.log(Level.INFO, "Check success message");
        Assert.assertEquals("Success message text is missing",
                text,
                contactUsPage.checkSuccessMessage());

    }


}
