package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.HomePage;
import pages.NavigationBar;
import pages.PopupHandler;

import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class HomeSteps {
    //    WebDriver driver;
    HomePage homepage;
    NavigationBar navigationBar;

    @Given("Being on Home page")
    public void beingOnHomePage() {
        driver.get(HomePage.HOME_URL);
        logger.log(Level.INFO, "Being on Home page");
        homepage = new HomePage(driver); // new home page object instance
    }
    @When("Click on logo")
    public void clickOnLogo() {
//        homepage.handlePopupDialog();
        homepage.clickLogo();
    }

    @Then("The home page URL is {string}")
    public void checkHomePageURL(String text) {
        logger.log(Level.INFO, "Check home page URL");
        Assert.assertEquals("Invalid URL",
                text,
                driver.getCurrentUrl());
    }

    @When("Click on Home button")
    public void clickOnHomeButton() {
        navigationBar = new NavigationBar(driver);
        navigationBar.clickHomeButton();
    }

    @Then("Navigation bar displays all {int} options in navigation bar")
    public void navigationBarDisplaysAllOptions(int i) {
        Assert.assertEquals("There are no 8 options in navigation bar",
                i,
                navigationBar.countButtons());
    }

    @When("Click on left control")
    public void clickOnLeftControl() {
        homepage.clickLeftControlSlider();
    }

    @Then("Number of slides is {int}")
    public void numberOfSlidesIs(int i) {
        Assert.assertEquals("There are no 3 options in navigation bar",
                i,
                homepage.countSliderButtons());
    }

    @Then("Slider {string} is correct")
    public void sliderTextIsCorrect(String text) {
        logger.log(Level.INFO, "Check slider string available");
        Assert.assertEquals("The text doesn't exist or is incorrect",
                text,
                homepage.checkTextSliderFull());
    }
    @And("Image slides are displayed")
    public void imageSlidesAreDisplayed() {
        Assert.assertTrue("Image slides missing",
                homepage.checkImageSlider());
    }

    @When("Click on Test Cases button in slider")
    public void clickOnTestCasesButtonInSlider() {
        homepage.clickSliderTestCasesButton();
    }

    @Then("Test cases page opens {string}")
    public void testCasesPageOpens(String text) {
        logger.log(Level.INFO, "Check Test Cases page URL");
        Assert.assertEquals("Invalid URL",
                text,
                HomePage.TEST_CASES_URL);
    }

    @When("Click on APIs list button in slider")
    public void clickOnAPIsListButtonInSlider() {
        homepage.clickSliderApiListButton();
    }

    @Then("Api list page opens {string}")
    public void apiListPageOpens(String text) {
        logger.log(Level.INFO, "Check API tests page URL");
        Assert.assertEquals("Invalid URL",
                text,
                HomePage.API_TEST_URL);
    }


}

