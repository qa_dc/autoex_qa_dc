package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertTrue;

public class ApiRestSteps {

    //API test URL:
    // https://restful-booker.herokuapp.com/apidoc/#api-Booking
    private Response response;

    @Given("A valid API endpoint for booking list")
    public void aValidApiEndpointForBookingList() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com";
    }

    @When("Send a GET request")
    public void sendAGetRequest() {
        response = given().contentType("application/json")
                .when()
                .get("/booking");
    }

    @Then("Receive a status code {int}")
    public void receiveAStatusCode(int status) {
        response.then().statusCode(status);
    }


    @Given("A valid API endpoint for booking")
    public void aValidAPIEndpointForBooking() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com/";
    }

    @When("Send a GET request for booking with ID = {int}")
    public void sendAGetRequestForBookingWithID(int bookingId) {
        response = given().contentType("application/json")
                .when()
                .get("booking/" + bookingId);
    }

    @Then("Status code is {int}")
    public void statusCodeIs(int status) {
        response.then().log().body().statusCode(status);
    }


    @Then("Booking should have first name {string} and last name {string} and check-in date {string}")
    public void bookingFirstLastNameAndDateCorrect(String fName, String lName, String date) {
        response.then().body("firstname", equalTo(fName))
                .and().body("lastname", equalTo(lName))
                .and().body("bookingdates.checkin", equalTo(date));
    }

    @When("Send a GET request for booking with incorrect ID = {int}")
    public void sendAGETRequestForBookingWithIncorrectID(int wrongBookingId) {
        response = given().contentType("application/json")
                .when()
                .get("booking/" + wrongBookingId);
    }

    @Then("Status code should be {int}")
    public void statusCodeShouldBe(int status) {
        response.then().log().body().statusCode(status);
    }

    @And("Response body should be {string}")
    public void responseBodyShouldBe(String text) {
        response.then().log().body().statusCode(404)
                .and().body(equalTo(text));
    }

    @When("Send a POST request to create a booking")
    public void sendAPostTRequestToCreateABooking() {
        Map<Object, Object> booking = new HashMap<>();
        booking.put("firstname", "Jimmy");
        booking.put("lastname", "Douglas");
        booking.put("totalprice", 120);
        booking.put("depositpaid", "true");
        booking.put("additionalneeds", "Breakfast");

        Map<Object, Object> bookingdates = new HashMap<>();
        bookingdates.put("checkin", "2019-01-01");
        bookingdates.put("checkout", "2020-01-01");

        booking.put("bookingdates", bookingdates);
        response = RestAssured.given().contentType("application/json").body(booking)
                .when().post("/booking");
    }

    @And("Response body should contain the booking firstname details")
    public void responseBodyShouldContainTheBookingDetails() {
        assertTrue(response.getBody().asString().contains("Jimmy"));
    }

    @And("Authorization header with value {string}")
    public void authorizationHeaderWithValue(String text) {
        given().header("Authorization", text).contentType("application/json");
    }

    @When("I send a DELETE request to delete a booking with ID = {int}")
    public void iSendADELETERequestToDeleteABookingWithID(int bookingId) {
        response = given().contentType("application/json")
                .when().delete("booking/" + bookingId);
    }
}
