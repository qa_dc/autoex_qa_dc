package api;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class ApiRestTest {

    //API test URL:
    // https://restful-booker.herokuapp.com/apidoc/#api-Booking
    @Test
    public void verifyBookingsListRequestStatus200() {
        given().contentType("application/json")
                .when().get("https://restful-booker.herokuapp.com/booking")
                .then().statusCode(200);
    }

    @Test
    public void verifyBookingById() {
        int bookingId = 12;
        given().contentType("application/json")
                .when().get("https://restful-booker.herokuapp.com/booking/" + bookingId)
                .then().log().body().statusCode(200);
    }

    @Test
    public void verifyBookingFirstAndLastNameIsCorrect() {
        given().contentType("application/json")
                .when().get("https://restful-booker.herokuapp.com/booking/12")
                .then().log().body().statusCode(200)
                .and().body("firstname", equalTo("John"))
                .and().body("lastname", equalTo("Smith"))
                .and().body("bookingdates.checkin", equalTo("2018-01-01"));
    }

    @Test
    public void verifyGetRequestWithIncorrectIdReturns404() {
        int bookingId = 1233;
        given().contentType("application/json")
                .when().get("https://restful-booker.herokuapp.com/booking/" + bookingId)
                .then().log().body().statusCode(404)
                .and().body(equalTo("Not Found"));
    }

    @Test
    public void verifyPostRequestReturns200(){
        Map<Object,Object> booking = new HashMap<>();
        booking.put("firstname", "Jimmy");
        booking.put("lastname", "Douglas");
        booking.put("totalprice", 120);
        booking.put("depositpaid", "true");
        booking.put("additionalneeds", "Breakfast");

        Map<Object,Object> bookingdates = new HashMap<>();
        bookingdates.put("checkin", "2019-01-01");
        bookingdates.put("checkout", "2020-01-01");

        booking.put("bookingdates", bookingdates);

        given().contentType("application/json").body(booking)
                .when().post("https://restful-booker.herokuapp.com/booking")
                .then().log().body().statusCode(200)
                .body("booking.firstname", equalTo("Jimmy"));
    }

    @Test
    public void verifyDeleteBookingReturns405() {
        int bookingId = 2071;
        String authorizationHeader = "Basic YWRtaW46cGFzc3dvcmQxMjM=";
        given().header("Authorization", authorizationHeader)
                .contentType("application/json")
                .when().delete("https://restful-booker.herokuapp.com/booking/"+ bookingId)
                .then().log().body().statusCode(405);
    }


}
